package plugin

import (
	"errors"
	"fmt"
	"time"

	"gopkg.in/yaml.v3"
	"gitee.com/alemond/log"
)

var (
	SetUpTimeout = 3 * time.Second

	MaxPlugins = 1000
)

var (
	// plugins 统一插件map:  plugin type => factory { plugin name => plugin factory}
	plugins = make(map[string]map[string]PluginFactory)
	done    = make(chan struct{})
)

type PluginFactory interface {
	Type() string
	SetUp(name string, dec Decoder) error
}

type Decoder interface {
	Decode(cfg interface{}) error
}

func WaitForDone(timeout time.Duration) bool {
	select {
	case <-done:
		return true
	case <-time.After(timeout):
	}
	return false
}

type YamlDecoder struct {
	Node *yaml.Node
}

func (d *YamlDecoder) Decode(cfg interface{}) error {
	if d.Node == nil {
		return errors.New("yaml node empty")
	}

	return d.Node.Decode(cfg)
}

func Register(name string, f PluginFactory) {
	factories, ok := plugins[f.Type()]
	if !ok {
		plugins[f.Type()] = map[string]PluginFactory{
			name: f,
		}
		return
	}

	factories[name] = f
}

func Get(typ string, name string) PluginFactory {
	factories, ok := plugins[typ]
	if !ok {
		return nil
	}
	return factories[name]
}

//PluginConfig 插件的统一配置，和plugins结构相同 plugin type => factory { plugin name => plugin config}
type PluginConfig map[string]map[string]yaml.Node

func (c PluginConfig) SetUp() error {
	var (
		pluginChan  = make(chan Info, MaxPlugins) // 插件初始化队列
		setupStatus = make(map[string]bool)       // 插件初始化状态, plugin key => true 初始化完成
	)

	// 将配置文件对应的插件逐个取出，并放进channel队列中
	for typ, factories := range c {
		for name, cfg := range factories {
			factory := Get(typ, name)
			if factory == nil {
				log.Debugf("plugin %s:%s no registered or imported, did not configure\n", typ, name)
				continue
			}
			p := Info{
				factory: factory,
				typ:     typ,
				name:    name,
				cfg:     cfg,
			}
			select {
			case pluginChan <- p:
			default:
				return fmt.Errorf("plugin number exceed limit:%d", len(pluginChan))
			}
			setupStatus[p.Key()] = false
		}
	}

	// 从channel队列中取出插件并初始化
	num := len(pluginChan)
	for num > 0 {
		for i := 0; i < num; i++ {
			p := <-pluginChan
			if err := p.SetUp(); err != nil {
				return err
			}
			setupStatus[p.Key()] = true
		}
		num = len(pluginChan)
	}

	select {
	case <-done:
	default:
		close(done)
	}

	return nil
}

type Info struct {
	factory PluginFactory
	typ     string
	name    string
	cfg     yaml.Node
}

func (p Info) SetUp() error {
	var (
		ch  = make(chan struct{})
		err error
	)

	go func() {
		err = p.factory.SetUp(p.name, &YamlDecoder{Node: &p.cfg})
		close(ch)
	}()

	select {
	case <-ch:
	case <-time.After(SetUpTimeout):
		return fmt.Errorf("setup plugin %s timeout", p.Key())
	}
	if err != nil {
		return fmt.Errorf("setup plugin %s error: %v", p.Key(), err)
	}
	return nil
}

func (p Info) Key() string {
	return fmt.Sprintf("%s-%s", p.typ, p.name)
}
