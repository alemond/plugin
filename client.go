package plugin

import (
	"sync"
)

var (
	clients   = make(map[string]Client)
	muxClient = sync.RWMutex{}
)

type Client interface {
}

func RegisterClient(name string, client Client) {
	if client == nil {
		panic("client: register nil client")
	}
	if name == "" {
		panic("client: register empty name of client ")
	}

	muxClient.Lock()
	clients[name] = client
	muxClient.Unlock()
}

func GetClient(name string) Client {
	muxClient.RLock()
	t := clients[name]
	muxClient.RUnlock()

	return t
}
