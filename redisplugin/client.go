package redisplugin

import (
	"gitee.com/alemond/plugin"

	redigo "github.com/gomodule/redigo/redis"
)

type RedisClient struct {
	conn redigo.Conn
}

var NewRedisProxy = func(name string) RedisClient {
	if name == "" {
		name = pluginName
	}
	
	c := plugin.GetClient(name)
	return c.(RedisClient)
}

func (c *RedisClient) Get(key string) (string, error) {
	reply, err := c.conn.Do("GET", key)
	if err != nil {
		return "", err
	}

	buf := reply.([]byte)
	return string(buf),nil
}

func (c *RedisClient) Set(key string, value string) error {
	_, err := c.conn.Do("SET", key, value)
	if err != nil {
		return err
	}

	return nil
}

func (c *RedisClient) Eval(script string, kc int, args ...interface{}) (interface{}, error) {
	lua := redigo.NewScript(kc,script)
	reply, err := lua.Do(c.conn,args...)
	if err != nil {
		return nil,err
	}

	return reply,nil
}
