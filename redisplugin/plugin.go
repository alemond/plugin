package redisplugin

import (
	"gitee.com/alemond/plugin"

	"gitee.com/alemond/log"
	redigo "github.com/gomodule/redigo/redis"
)

const (
	pluginType = "redis"
	pluginName = "redis"
)

func init() {
	plugin.Register(pluginName, &RedisPlugin{})
}

type RedisPlugin struct {
}

type Config struct {
	Address string `yaml:"address"`
	Passwd  string `yaml:"passwd"`
}

func (rp *RedisPlugin) Type() string {
	return pluginType
}

func (rp *RedisPlugin) SetUp(name string, cfgDecoder plugin.Decoder) error {
	log.Debugf("redis name:%v\n", name)
	var config Config
	if err := cfgDecoder.Decode(&config); err != nil {
		return err
	}

	conn, err := redigo.Dial("tcp", config.Address, redigo.DialPassword(config.Passwd))
	if err != nil {
		log.Errorf("dial redis name:%v, err:%v\n", name, err)
		return err
	}

	redisClient := RedisClient{conn: conn}

	log.Debugf("register redis client,addr:%s,passwd:%v\n", config.Address, config.Passwd)
	plugin.RegisterClient(name, redisClient)
	return nil
}
