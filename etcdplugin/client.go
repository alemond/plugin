package etcdplugin

import (
	"gitee.com/alemond/plugin"
	"gitee.com/alemond/log"
	"context"
	"encoding/json"
	"fmt"

	etcdv3 "go.etcd.io/etcd/client/v3"
)

type Registry struct {
	client *etcdv3.Client
	kv     etcdv3.KV
}

var NewRegistry = func(name string) Registry {
	if name == "" {
		name = pluginName
	}

	c := plugin.GetClient(name)
	return c.(Registry)
}

func (r *Registry) Put(key string, value string) (*etcdv3.PutResponse, error) {

	rsp, err := r.kv.Put(context.TODO(), key, value)
	if err != nil {
		log.Errorf("err:%v\n", err)
		return nil, err
	}

	return rsp, nil
}

func (r *Registry) Get(key string) (string, error) {
	log.DPanicf("get key:%v\n", key)
	rsp, err := r.kv.Get(context.TODO(), key)
	if err != nil {
		log.Errorf("err:%v\n",err)
		return "", err
	}

	srsp,_ := json.Marshal(rsp)
	log.Debugf("rsp:",string(srsp))

	if rsp == nil{
		log.Debugf("rsp nil")
		return "",fmt.Errorf("key not found")
	}

	if len(rsp.Kvs) == 0{
		log.Debugf("get no kvs")
		return "", nil
	}

	log.Debugf("value:%v\n",string(rsp.Kvs[0].Value))
	return string(rsp.Kvs[0].Value), nil
}

func (r *Registry) Delete(key string) error {
	_, err := r.kv.Delete(context.TODO(), key)
	if err != nil {
		log.Errorf("err:%v\n",err)
		return  err
	}

	return nil
}


