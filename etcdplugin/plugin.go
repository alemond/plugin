package etcdplugin

import (
	"time"

	"gitee.com/alemond/log"
	"gitee.com/alemond/plugin"

	etcdv3 "go.etcd.io/etcd/client/v3"
)

const (
	pluginType = "etcd"
	pluginName = "etcd"
)

func init() {
	plugin.Register(pluginName, &etcdPlugin{})
}

type etcdPlugin struct {
}

type Config struct {
	Endpoint []string `yaml:"endpoints"`
	Timeout  int64    `yaml:"timeout"`
}

func (ep *etcdPlugin) Type() string {
	return pluginType
}

func (ep *etcdPlugin) SetUp(name string, cfgDecoder plugin.Decoder) error {
	log.Debugf("etcd name:%v\n", name)
	var config Config
	if err := cfgDecoder.Decode(&config); err != nil {
		return err
	}

	cli, err := etcdv3.New(etcdv3.Config{
		Endpoints:   config.Endpoint,
		DialTimeout: time.Duration(config.Timeout) * time.Millisecond,
	})
	if err != nil {
		log.Errorf("new etcd client %v err: %v", name, err)
		return err
	}

	kv := etcdv3.NewKV(cli)

	registry := Registry{
		client: cli,
		kv:     kv,
	}

	log.Debugf("register etcd client,addr:%s,timeout:%v\n", config.Endpoint, time.Duration(config.Timeout)*time.Millisecond)
	plugin.RegisterClient(name, registry)

	return nil
}
